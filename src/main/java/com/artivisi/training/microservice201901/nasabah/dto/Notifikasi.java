package com.artivisi.training.microservice201901.nasabah.dto;

import lombok.Data;

@Data
public class Notifikasi {
    private String type;
    private String from;
    private String to;
    private String subject;
    private String message;
}
