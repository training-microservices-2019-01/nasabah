package com.artivisi.training.microservice201901.nasabah.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Profile("!unsecure")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class KonfigurasiResourceServer  extends WebSecurityConfigurerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(KonfigurasiResourceServer.class);

    @Value("${security.oauth2.resourceserver.jwt.jwk-set-uri}")
    private String jwkSetUri;

    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .oauth2ResourceServer()
                .jwt()
                .jwtAuthenticationConverter(grantedAuthoritiesExtractor())
                .jwkSetUri(jwkSetUri);
    }

    private Converter<Jwt, AbstractAuthenticationToken> grantedAuthoritiesExtractor() {
        return new JwtAuthenticationConverter(){
            protected Collection<GrantedAuthority> extractAuthorities(Jwt jwt) {
                LOGGER.debug("JWT Claims : {}", jwt.getClaims());

                Collection<String> authorities = (Collection<String>)
                        jwt.getClaims().get("authorities");

                LOGGER.debug("Extracted Authorities : {}", authorities);

                /*
                List<GrantedAuthority> hasil = new ArrayList<>();
                for (String auth : authorities) {
                    hasil.add(new SimpleGrantedAuthority(auth));
                }
                return hasil;
                */

                return authorities.stream()
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList());
            }
        };
    }
}

