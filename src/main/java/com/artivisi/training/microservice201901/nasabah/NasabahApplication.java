package com.artivisi.training.microservice201901.nasabah;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
public class NasabahApplication {

	public static void main(String[] args) {
		SpringApplication.run(NasabahApplication.class, args);
	}

}

