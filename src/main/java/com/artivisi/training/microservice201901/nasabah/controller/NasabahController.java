package com.artivisi.training.microservice201901.nasabah.controller;

import com.artivisi.training.microservice201901.nasabah.config.AppPermissions;
import com.artivisi.training.microservice201901.nasabah.dao.NasabahDao;
import com.artivisi.training.microservice201901.nasabah.entity.Nasabah;
import com.artivisi.training.microservice201901.nasabah.service.KafkaSenderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/nasabah")
public class NasabahController {
    private static final Logger LOGGER = LoggerFactory.getLogger(NasabahController.class);

    @Autowired private NasabahDao nasabahDao;

    @Autowired private KafkaSenderService kafkaSenderService;

    @PreAuthorize("hasAuthority('SCOPE_entri_data')")
    @GetMapping("/semua")
    public Iterable<Nasabah> dataNasabahTanpaPaging() {
        LOGGER.info("Mengambil data nasabah");
        return nasabahDao.findAll();
    }

    @PreAuthorize(AppPermissions.PERMISSION_VIEW_TRANSAKSI)
    @GetMapping("/")
    public Page<Nasabah> dataSemuaNasabah(Pageable page){
        return nasabahDao.findAll(page);
    }

    @PreAuthorize(AppPermissions.PERMISSION_EDIT_TRANSAKSI)
    @GetMapping("/{id}")
    public Nasabah cariById(@PathVariable(name = "id") Nasabah nasabah){
        return nasabah;
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void insert(@RequestBody @Valid Nasabah nasabah){
        nasabahDao.save(nasabah);
        kafkaSenderService.kirimNotifikasiNasabahBaru(nasabah);
    }

    @PreAuthorize("hasAuthority('DELETE_TRANSAKSI')")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable(name = "id") String id){
        nasabahDao.deleteById(id);
    }
}
