package com.artivisi.training.microservice201901.nasabah.config;

public abstract class AppPermissions {
    public static final String PERMISSION_VIEW_TRANSAKSI = "hasAuthority('VIEW_TRANSAKSI')";
    public static final String PERMISSION_EDIT_TRANSAKSI = "hasAuthority('EDIT_TRANSAKSI')";
}
