package com.artivisi.training.microservice201901.nasabah.service;

import com.artivisi.training.microservice201901.nasabah.dto.Notifikasi;
import com.artivisi.training.microservice201901.nasabah.entity.Nasabah;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaSenderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaSenderService.class);

    @Value("${kafka.topic.notification}")
    private String topicNotification;

    @Autowired private ObjectMapper objectMapper;
    @Autowired private KafkaTemplate<String, String> kafkaTemplate;

    public void kirimNotifikasiNasabahBaru(Nasabah nasabah) {
        try {
            Notifikasi notif = new Notifikasi();
            notif.setFrom("aplikasi.nasabah@gmail.com");
            notif.setTo(nasabah.getEmail());
            notif.setSubject("Selamat bergabung");
            notif.setMessage("Terima kasih telah bergabung dengan layanan kami");

            String jsonNotif = objectMapper.writeValueAsString(notif);
            LOGGER.debug("JSON notif : {}", jsonNotif);

            kafkaTemplate.send(topicNotification, jsonNotif);
        } catch (Exception err) {
            LOGGER.error(err.getMessage(), err);
        }
    }

}
