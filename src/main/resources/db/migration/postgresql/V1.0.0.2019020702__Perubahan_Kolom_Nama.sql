alter table nasabah
add column nama_depan varchar (255),
add column nama_belakang varchar (255);

-- harusnya di sini migrasi dulu data nama ke nama_depan + nama_belakang

alter table nasabah
drop column nama;

alter table nasabah
alter column nama_depan set not null;

alter table nasabah
alter column nama_belakang set not null;