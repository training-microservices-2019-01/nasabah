create table nasabah (
  id varchar(36),
  nomor varchar (100) not null,
  nama varchar (255) not null,
  email varchar(255) not null,
  no_hp varchar(50) not null,
  primary key (id),
  unique (nomor),
  unique (email),
  unique (no_hp)
);